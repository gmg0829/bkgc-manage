<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>角色列表</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="description" content="">
    <meta name="keywords" content="coco bootstrap template, coco admin, bootstrap,admin template, bootstrap admin,">
    <meta name="author" content="Huban Creative">

    <!-- Base Css Files -->
    <link href="../css/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="../css/bootstrap.min.css" rel="stylesheet" />
    <link href="../css/font-awesome.min.css" rel="stylesheet" />
    <link href="../css/component.css" rel="stylesheet" />
    <link href="../css/datepicker.css" rel="stylesheet" />
    <link href="../css/summernote.css" rel="stylesheet" type="text/css" />
    <link href="../css/sortable-theme-bootstrap.css" rel="stylesheet" />
    <link href="../css/all.css" rel="stylesheet" />
    <link href="../css/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../css/dataTables.tableTools.css" rel="stylesheet" type="text/css" />
    <link href="../css/style.css" rel="stylesheet" type="text/css" />
    <link href="../css/style-responsive.css" rel="stylesheet" />
</head>

<body class="fixed-left">
<div id="wrapper">
 <#include "./common/common.ftl" encoding="UTF-8" parse=true>
 <!--右侧栏-->
    <div class="content-page">
        <div class="content">
            <div class="page-heading">
                <h1><i class='fa fa-table'></i>福包账户</h1>
            </div>

            <div class="row">

                <div class="col-md-12">
                    <div class="widget">
                        <div class="widget-content">
                            <div class="data-table-toolbar">
                                <div class="row">
                                    <div class="col-md-12" >
                                        <form class="form-inline" role="form">
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="email" class="form-control" placeholder="email" id="email">
                                            </div>
                                            <div class="form-group">
                                                <label for="phone">phone</label>
                                                <input type="text" class="form-control" placeholder="phone" id="phone">
                                            </div>
                                            <div class="form-group">
                                                <label>起始时间</label>
                                                <input type="text" class="form-control datepicker-input">
                                            </div>
                                            <div class="form-group">
                                                <label>结束时间</label>
                                                <input type="text" class="form-control datepicker-input">
                                            </div>
                                            <div class="form-group">
                                                <label>彩票</label>
                                                <select class="form-control">
                                                    <option>点石成金</option>
                                                    <option>麻辣六</option>
                                                </select>
                                            </div>
                                            <button type="submit" class="btn btn-default">搜索</button>
                                            <button type="submit" class="btn btn-default">导出报表</button>
                                        </form>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-9" >
                                    </div>
                                    <div class="col-md-3">
                                        <div class="toolbar-btn-action">
                                            <a class="btn btn-success"><i class="fa fa-plus-circle"></i> Add new</a>
                                            <a class="btn btn-primary"><i class="fa fa-refresh"></i> Update</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <form class='form-horizontal' role='form'>
                                    <table id="datatables-3" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>First name</th>
                                            <th>Last name</th>
                                            <th>Position</th>
                                            <th>Office</th>
                                            <th>Salary</th>
                                            <th data-sortable="false">Option</th>
                                        </tr>
                                        </thead>

                                      <#--  <tfoot>
                                        <tr>
                                            <th colspan="5" style="text-align:right">Total:</th>
                                            <th>$1234</th>
                                        </tr>
                                        </tfoot>-->

                                        <tbody>
                                                  <tr>
                                                        <td>123</td>
                                                        <td>456</td>
                                                        <td>sss</td>
                                                        <td>eee</td>
                                                        <td>111</td>
                                                        <td>
                                                                <a data-toggle="tooltip" title="Edit" class="btn btn-default"><i class="fa fa-edit"></i></a>
                                                        </td>
                                                    </tr>
                                                   <tr>
                                                       <td>123</td>
                                                       <td>456</td>
                                                       <td>sss</td>
                                                       <td>eee</td>
                                                       <td>111</td>
                                                       <td>
                                                               <a data-toggle="tooltip" title="Edit" class="btn btn-default"><i class="fa fa-edit"></i></a>
                                                       </td>
                                                   </tr>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
<div class="md-overlay"></div>
<script>
    var resizefunc = [];
</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../js/jquery-1.11.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="../js/detect.js"></script>
<script src="../js/fastclick.js"></script>
<script src="../js/jquery.slimscroll.js"></script>
<script src="../js/jquery-sparkline.js"></script>
<script src="../js/sortable.min.js"></script>
<script src="../js/bootstrap.file-input.js"></script>
<script src="../js/bootstrap-select.min.js"></script>
<script src="../js/icheck.min.js"></script>
<script src="../js/prettify.js"></script>
<script src="../js/init.js"></script>
<script src="../js/jquery.dataTables.min.js"></script>
<script src="../js/dataTables.bootstrap.js"></script>
<script src="../js/dataTables.tableTools.min.js"></script>
<script src="../js/datatables.js"></script>
<script src="../js/bootstrap-datepicker.js"></script>
</body>

</html>