<style>
.info_{margin-top: 20px;} .text_{margin-top: 25px;}
</style>
<!-- 顶部-->
<div class="topbar">
    <!--顶部左-->
    <div class="topbar-left">
        <div class="logo">
            <h1><a href="#">北控国彩</a></h1>
        </div>
        <button class="button-menu-mobile open-left">
            <i class="fa fa-bars"></i>
        </button>
    </div>
    <!--顶部右-->
    <div class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="navbar-collapse2">
                <ul class="nav navbar-nav navbar-right top-navbar">
                    <!--通知-->
                    <li class="dropdown iconify hide-phone">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i><span class="label label-danger absolute">3</span></a>
                        <ul class="dropdown-menu dropdown-message">
                            <li class="dropdown-header notif-header"><i class="icon-mail-2"></i>新消息</li>
                            <li class="unread">
                                <a href="#" class="clearfix">
                                    <img src="../../images/logo.png" class="xs-avatar ava-dropdown" alt="Avatar">
                                    <strong>John Doe</strong><i class="pull-right msg-time">5 minutes ago</i><br />
                                    <p>Duis autem vel eum iriure dolor in hendrerit ...</p>
                                </a>
                            </li>

                        </ul>
                    </li>
                    <!--头像-->
                    <li class="dropdown topbar-profile">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="rounded-image topbar-profile-image"><img src="../../images/logo.png"></span><strong>${username}</strong> <i class="fa fa-caret-down"></i></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#">个人信息</a>
                            </li>
                            <li>
                                <a href="#">修改密码</a>
                            </li>
                            <li>
                                <a href="#">账户设置</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#"><i class="icon-help-2"></i>帮助</a>
                            </li>
                            <li>
                                <a class="md-trigger" data-modal="logout-modal" href="/logOut"><i class="icon-logout-1"></i>退出</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>






