<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>角色列表</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="description" content="">
    <meta name="keywords" content="coco bootstrap template, coco admin, bootstrap,admin template, bootstrap admin,">
    <meta name="author" content="Huban Creative">
    <link href="../../css/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="../../css/bootstrap/bootstrap.min.css" rel="stylesheet" />
    <link href="../../css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../css/component.css" rel="stylesheet" />
    <link href="../../css/datepicker.css" rel="stylesheet" />
    <link href="../../css/summernote.css" rel="stylesheet" type="text/css" />
    <link href="../../css/sortable-theme-bootstrap.css" rel="stylesheet" />
    <link href="../../css/all.css" rel="stylesheet" />
    <link href="../../css/style.css" rel="stylesheet" type="text/css" />
    <link href="../../css/style-responsive.css" rel="stylesheet" />
    <link href="../../css/datatable/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../../css/datatable/dataTables.tableTools.css" rel="stylesheet" type="text/css" />
</head>

<body class="fixed-left">
<div id="wrapper">
<#include "../common/common.ftl" encoding="UTF-8" parse=true>
    <!-- 左侧栏 -->
    <div class="left side-menu" style="background:#FFE4E1;">
        <div class="sidebar-inner slimscrollleft">
            <div class="clearfix"></div>
            <!--- 左上-->
            <div class="profile-info info_">
                <div class="col-xs-4">
                    <a href="profile.html" class="rounded-image profile-image"><img src="../../images/logo.png"></a>
                </div>
                <div class="col-xs-8 text_">
                    <div class="profile-text">Hello,<b>${username}</b></div>
                </div>
            </div>
            <div class="clearfix"></div>
            <hr class="divider" />
            <div class="clearfix"></div>
            <!--左下 菜单栏-->
            <div id="sidebar-menu">
                <ul>
                <@shiro.hasPermission name="secManage">
                    <li class='has_sub'>
                        <a href='javascript:void(0);'><i class='icon-home-3'></i><span>安全中心</span>
                            <span class="pull-right"><i class="fa fa-angle-down"></i></span></a>
                        <ul>
                            <@shiro.hasPermission name="userManage">
                                <li>
                                    <a href='/user/userList'>
                                        <span>用户管理</span></a>
                                </li>
                            </@shiro.hasPermission>
                            <@shiro.hasPermission name="roleManage">
                                <li>
                                    <a href='/rolePermission/roleListIndex' class='active'><span>角色管理</span></a>
                                </li>
                            </@shiro.hasPermission>
                        </ul>
                    </li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="accountCenter">
                    <li class='has_sub'>
                        <a href='javascript:void(0);'><i class='icon-home-3'></i><span>账户中心</span>
                            <span class="pull-right"><i class="fa fa-angle-down"></i></span></a>
                        <ul>
                            <@shiro.hasPermission name="personalAcc">
                                <li>
                                    <a href=''>
                                        <span>个人账户</span></a>
                                </li>
                            </@shiro.hasPermission>
                            <@shiro.hasPermission name="retailerAcc">
                                <li>
                                    <a href=''><span>商户账户</span></a>
                                </li>
                            </@shiro.hasPermission>
                            <@shiro.hasPermission name="enterpriseAcc">
                                <li>
                                    <a href=''><span>企业账户</span></a>
                                </li>
                            </@shiro.hasPermission>
                        </ul>
                    </li>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="promotionManage">
                    <li class='has_sub'>
                        <a href='javascript:void(0);'><i class='icon-home-3'></i><span>福包管理</span>
                            <span class="pull-right"><i class="fa fa-angle-down"></i></span></a>
                        <ul>
                            <@shiro.hasPermission name="generalBP">
                                <li>
                                    <a href=''>
                                        <span>普通福包</span></a>
                                </li>
                            </@shiro.hasPermission>
                            <@shiro.hasPermission name="advertiseBP">
                                <li>
                                    <a href=''><span>广告福包</span></a>
                                </li>
                            </@shiro.hasPermission>
                            <@shiro.hasPermission name="charityBP">
                                <li>
                                    <a href=''><span>公益福包</span></a>
                                </li>
                            </@shiro.hasPermission>
                        </ul>
                    </li>
                </@shiro.hasPermission>
                <#-- <li class='has_sub'>
                     <a href='javascript:void(0);'><i class='icon-megaphone'></i><span>阿里</span> <span class="pull-right"><i class="fa fa-angle-down"></i></span></a>
                     <ul>
                         <li>
                             <a href='/grid1'><span>支付宝</span></a>
                         </li>
                         <li class='has_sub'>
                             <a href='javascript:void(0);'><span>蚂蚁森林</span> <span class="pull-right"><i class="fa fa-angle-down"></i></span></a>
                             <ul>
                                 <li>
                                     <a href='javascript:void(0);'><span>浇水</span></a>
                                 </li>
                             </ul>
                         </li>
                         <li class='has_sub'>
                             <a href='javascript:void(0);'><span>飞猪</span> <span class="pull-right"><i class="fa fa-angle-down"></i></span></a>
                             <ul>
                                 <li class='has_sub'>
                                     <a href='javascript:void(0);'><span>火车票</span> <span class="pull-right"><i class="fa fa-angle-down"></i></span></a>
                                     <ul>
                                         <li>
                                             <a href='javascript:void(0);'><span>买票</span></a>
                                         </li>
                                         <li>
                                             <a href='javascript:void(0);'><span>退票</span></a>
                                         </li>
                                     </ul>
                                 </li>
                             </ul>
                         </li>
                     </ul>
                 </li>-->
                </ul>
                <div class="clearfix"></div>
            </div>
        </div>

    </div>
    <!--右侧栏-->
    <div class="content-page">
        <div class="content">
            <div class="page-heading">
                <h1><i class='fa fa-table'></i>角色列表</h1>
            </div>

            <div class="row">

                <div class="col-md-12">
                    <div class="widget">
                        <div class="widget-header">
                            <h2><strong>角色</strong>列表</h2>
                            <div class="additional-btn">
                                <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                                <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                                <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
                            </div>
                        </div>
                        <div class="widget-content">
                            <br>
                            <div class="table-responsive">
                                <form class='form-horizontal' role='form'>
                                    <table id="datatables-2" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>角色名称</th>
                                            <th>状态</th>
                                            <th>操作</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <#list roles as role>
                                           <tr>
                                                <td>${role.name!}(${role.description!})</td>
                                                <td>${(role.deleted==1)?string('不可用','可用')}</td>
                                                <td>
                                                     <a data-toggle="tooltip" title="编辑" class="btn btn-default" style="margin-left: 10px;" href="/rolePermission/roleSet/${role.id}"><i class="fa fa-edit"></i></a>
                                                </td>
                                           </tr>
                                        </#list>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
<div class="md-overlay"></div>
<script>
    var resizefunc = [];
</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../../js/jquery/jquery-1.11.1.min.js"></script>
<script src="../../js/bootstrap/bootstrap.min.js"></script>
<script src="../../js/jquery/jquery-ui-1.10.4.custom.min.js"></script>
<script src="../../js/detect.js"></script>
<script src="../../js/fastclick.js"></script>
<script src="../../js/jquery/jquery.slimscroll.js"></script>
<script src="../../js/jquery/jquery-sparkline.js"></script>
<script src="../../js/sortable.min.js"></script>
<script src="../../js/bootstrap/bootstrap.file-input.js"></script>
<script src="../../js/bootstrap/bootstrap-select.min.js"></script>
<script src="../../js/icheck.min.js"></script>
<script src="../../js/prettify.js"></script>
<script src="../../js/init.js"></script>
<script src="../../js/bootstrap/bootstrap-datepicker.js"></script>
<script src="../../js/jquery/jquery.dataTables.min.js"></script>
<script src="../../js/datatable/dataTables.bootstrap.js"></script>
<script src="../../js/datatable/dataTables.tableTools.min.js"></script>
<script src="../../js/datatable/datatables.js"></script>
</body>

</html>