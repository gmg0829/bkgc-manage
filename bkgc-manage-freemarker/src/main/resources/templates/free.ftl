${username!}
${username?if_exists}
${username?default("admmin")}
${usrname!"admin"}

${"123<br>456"?html}    　　// 对字符串进行HTML编码，对html中特殊字符进行转义
${"str"?cap_first}    　　  // 使字符串第一个字母大写
${"Str"?lower_case}        // 将字符串转换成小写
${"Str"?upper_case}        // 将字符串转换成大写
${"str"?trim}              // 去掉字符串前后的空白字符

${"hello${emp.name!}"}     // 输出hello+变量名
${"hello"+emp.name!}       // 使用+号来连接，输出hello+变量名

<#assign str = "abcdefghijklmn"/>
// 方法1
${str?substring(0,4)}  // 输出abcd
// 方法2
${str[0]}${str[4]}    // 结果是ae
${str[1..4]}    　　　 // 结果是bcde
// 返回指定字符的索引
${str?index_of("n")}

${emp.date?string('yyyy-MM-dd')} //日期格式

<#if a==1>

    <#elseif a==2>

    <#else >

</#if>//判断
<div <#if a==2>class="test"</#if>></div>

<#list animals as animal>
    ${animal.name}
</#list>//遍历

<#include "index.ftl" encoding="UTF-8" parse=true>//包含页面

${emp.name?string.number}    　// 输出20
${emp.name?string.currency}    // ￥20.00
${emp.name?string.percent}     // 20%
${1.222?int} 　　　　　　　　　　 // 将小数转为int，输出1
<#setting number_format="percent"/>    // 设置数字默认输出方式('percent',百分比)
<#assign answer=42/>    　　　　　　　　 // 声明变量 answer 42
#{answer}    　　　　　　　　 // 输出 4,200%
${answer?string}    　　　　 // 输出 4,200%
${answer?string.number} 　　// 输出 42
${answer?string.currency}  // 输出 ￥42.00
${answer?string.percent} 　// 输出 4,200%
#{answer}    　　　　　　　　// 输出 42

mX:小数部分最小X位
MX:小数部分最大X位
<#assign x=2.582/>
<#assign y=4/>
#{x; M2}    // 输出2.58
#{y; M2}    // 输出4
#{x; m2}    // 输出2.58
#{y; m2}    // 输出4.0
#{x; m1M2}  // 输出2.58
#{x; m1M2}  // 输出4.0
${true?string('5','7')}
<#switch a>
   <#case a==1>
   <#break>
    <#default>
</#switch>

/ 集合长度判断
<#if empList?size != 0></#if> // 判断=的时候,注意只要一个=符号,而不是==
<#list 0..100 as i> 　　// 等效于java for(int i=0; i <= 100; i++)
　　${i}
</#list>

// sort_by：排序（升序）
<#list movies?sort_by("showtime") as movie></#list>
// sort_by：排序（降序）
<#list movies?sort_by("showtime")?reverse as movie></#list>

// 类似于jsp里的import,它导入文件，然后就可以在当前文件里使用被导入文件里的宏组件
<#import "index.ftl" as my>
<#assign text>[{"name":"guocai-shopkeeper","deleted":0},{"name":"guocai","deleted":0}]</#assign>
<#assign text>${roles}</#assign>

