package com.bkgc.manage.consumer;

import com.bkgc.manage.bean.OauthRole;
import com.bkgc.manage.bean.PermissionMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by beikongguocai on 2017/9/29.
 */
@Service
public class RolePermissionService {
    @Value("${module_id}")
    private String moduleId;
    @Value("${manage_admin}")
    private  String manage_admin;
    @Autowired
    RestTemplate restTemplate;

    public  List<OauthRole> getRoles(){
         List<OauthRole> roles = (List<OauthRole>)restTemplate.getForObject("http://SECURITY-CENTER/role/all?moduleId={1}",Object.class,moduleId);
        return roles;
    }
    public OauthRole getRoleById(String roleid){
        return restTemplate.getForObject("http://SECURITY-CENTER/role/get?roleId={1}",OauthRole.class,roleid);
    }
    public  List<PermissionMenu> getPermissions(){
        ParameterizedTypeReference<List<PermissionMenu>> responseType = new ParameterizedTypeReference<List<PermissionMenu>>(){};
        ResponseEntity<List<PermissionMenu>> user = restTemplate.exchange("http://SECURITY-CENTER/permission/get?roleId={1}",
                HttpMethod.GET, null, responseType,manage_admin);
        return user.getBody();
    }


}
