package com.bkgc.manage.consumer;

import com.bkgc.manage.bean.AuthUser;
import com.bkgc.manage.bean.OauthRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by gmg on 2017/10/10.
 */
@Service
public class UserService {
    @Autowired
    RestTemplate restTemplate;
    public List<AuthUser> getUsers(){
        List<AuthUser> authUsers = (List<AuthUser>)restTemplate.getForObject("http://SECURITY-CENTER/listAll",Object.class);
        return authUsers;
    }
}
