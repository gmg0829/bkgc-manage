package com.bkgc.manage.bean;

import java.util.List;

public class OauthRole {
    private Integer id;

    private String name;

    private Integer moduleId;

    private Integer deleted;

    private Integer isDefault;

    private String description;

    private List<PermissionMenu> permissions;

    public List<PermissionMenu> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<PermissionMenu> permissions) {
        this.permissions = permissions;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getModuleId() {
        return moduleId;
    }

    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Integer getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }
}