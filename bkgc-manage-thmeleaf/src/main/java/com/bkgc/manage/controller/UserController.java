package com.bkgc.manage.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by beikongguocai on 2017/10/10.
 */
@Controller
@RequestMapping(value = "/user")
public class UserController {
    @RequestMapping(value = "/userList")
    public String getUserList(){
        return "page/permission/userList";
    }
    @RequestMapping(value = "/userAdd")
    public String userAdd(){
        return "page/permission/userEdit";
    }
}
