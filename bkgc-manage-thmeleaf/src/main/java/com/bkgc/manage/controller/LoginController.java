package com.bkgc.manage.controller;

import java.io.FileWriter;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.bkgc.manage.consumer.ShiroService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LoginController {
	Logger logger= LoggerFactory.getLogger(this.getClass());
	
	@Value("${oauth_url}")
	private String oauth_url;
	
	 @RequestMapping(value="/login")
	 public String goLogin(){
		 return "login";
	 }

	@RequestMapping(value="/logOut")
	public String logOut(){
		return "login";
	}
	 @Autowired
	ShiroService shiroService;
	 @RequestMapping(value="/loginOn",method=RequestMethod.POST)
	 @ResponseBody
	 public int loginOn(String username,String password,HttpSession session,HttpServletRequest request) throws Exception{
	 	try{
			 logger.info("用户名"+username+"-----"+password);
			 SecurityUtils.getSubject().login(new UsernamePasswordToken(username,password));
			 session.setAttribute("username",username);
			 return 1;
		   }catch(Exception e){
			   e.printStackTrace();    
			   return 0;
		   }
	 }
	 
	 private void createNavFile(String path,String navStr){
		FileWriter writer;
        try {
            writer = new FileWriter(path);
            writer.write(navStr);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
	 }

}
