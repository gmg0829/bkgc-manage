package com.bkgc.manage.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.bkgc.manage.bean.OauthRole;
import com.bkgc.manage.bean.PermissionMenu;
import com.bkgc.manage.consumer.RolePermissionService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "/rolePermission")
public class RolePermissionController {
	@Autowired
	RolePermissionService rolePermissionService;

	@Value("${oauth_url}")
	private String oauth_url;

    @RequestMapping("/grid1")
	public String grid1() {
		return "page/grid1";
	}
	//角色列表
	@RequestMapping(value = "/roleListIndex")
	public String getRoleListIndex(HttpServletRequest request) {
        List<OauthRole> roles=rolePermissionService.getRoles();
        request.setAttribute("roles",roles);
		return "page/permission/roleList";
	}
	//查看角色详情
    @RequestMapping(value = "/roleSet/{roleid}")
    public String roleSet(@PathVariable("roleid") String roleid, HttpSession session) {
    	OauthRole oauthRole=rolePermissionService.getRoleById(roleid);
		session.setAttribute("oauthRole",oauthRole);
    	return "page/permission/roleSet";
    }
   //获取权限列表
   @RequestMapping(value = "/treeData")
   @ResponseBody
   public List<Map<String, Object>> getTreeData(){
	   List<Map<String, Object>> mapList = Lists.newArrayList();
	   List<PermissionMenu> permissionMenuList=rolePermissionService.getPermissions();
	   for (PermissionMenu permissionmenu : permissionMenuList) {
		  Map<String, Object> map = Maps.newHashMap();
		   map.put("id", permissionmenu.getId());
		   map.put("pId", permissionmenu.getParentId());
		   //*  map.put("pIds", permissionmenu.getParentIds());*//*
		   map.put("name", permissionmenu.getName());
		   mapList.add(map);

	   }
	   return mapList;

   }




}
