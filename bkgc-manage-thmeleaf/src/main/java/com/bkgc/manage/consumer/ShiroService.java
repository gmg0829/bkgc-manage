package com.bkgc.manage.consumer;

import com.bkgc.manage.bean.OauthRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by beikongguocai on 2017/9/27.
 */
@Service
public class ShiroService {
    @Autowired
    RestTemplate restTemplate;
    public OauthRole getRoleById(String roleid){
        return restTemplate.getForObject("http://SECURITY-CENTER/role/get?roleId={1}",OauthRole.class,roleid);
    }

}
